import React, { Component } from 'react';
import Rotas from './src/Routes';

export default class App extends React.Component {
  render() {
    return (
      <Rotas />
    );
  }
}
