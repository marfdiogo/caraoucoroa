import React from 'react';
import {
  StyleSheet
} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';

import Principal from './components/Principal';
import SobreJogo from './components/SobreJogo';
import OutrosJogos from './components/OutrosJogos';
import Resultado from './components/Resultado';

const Rotas = () => {
  return (
    <Router tintColor='white' navigationBarStyle={styles.statusBar} titleStyle={styles.titleTopPadrao}>
      <Scene key='root'>
        <Scene init key='principal' titleStyle={styles.titleTop} component={Principal} title="Cara ou Coroa" navigationBarStyle={{ backgroundColor: '#5eba7d' }}/>
        <Scene key='sobrejogo' component={SobreJogo} title="Sobre o jogo" />
        <Scene key='outrosjogos' component={OutrosJogos} title="Outros jogos" />
        <Scene key='resultado' component={Resultado} title="Resultado" />
      </Scene>
    </Router>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  statusBar: {
    backgroundColor: '#5eba7d'
  },
  titleTopPadrao: {
    color: '#fff',
    textAlign: 'right',
    flex: 1
  },
  titleTop: {
    textAlign: 'center',
    color: '#fff',
    flex: 1
  }
});

export default Rotas;
