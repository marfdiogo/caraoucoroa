import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class BarraPrincipal extends React.Component {
  render() {
    return (
      <View style={[styles.container, {backgroundColor: this.props.color}]}></View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 24
  },
});
