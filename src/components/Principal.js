import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';
import { Actions } from 'react-native-router-flux';

const logo = require('../imgs/logo.png');
const btnJogar = require('../imgs/botao_jogar.png');
const btnSobreJogo = require('../imgs/sobre_jogo.png');
const btnOutrosJogos = require('../imgs/outros_jogos.png');

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cenaPrincipal}>

          <View style={styles.apresentacaoJogo}>
            <Image source={logo} />
            <TouchableHighlight onPress={() => {Actions.resultado()}} underlayColor={0}>
              <Image source={btnJogar} />
            </TouchableHighlight>
          </View>

          <View style={styles.rodape}>
            <TouchableHighlight onPress={() => {Actions.sobrejogo()}} underlayColor={0}>
              <Image source={btnSobreJogo} />
            </TouchableHighlight>
            <TouchableHighlight onPress={() => {Actions.outrosjogos()}} underlayColor={0}>
              <Image source={btnOutrosJogos} />
            </TouchableHighlight>
          </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  cenaPrincipal: {
    flex: 1,
    backgroundColor: '#61BD8C'
  },
  apresentacaoJogo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rodape: {
    display: 'none',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
