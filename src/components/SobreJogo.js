import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

const textPrincipal = 'Informações do jogo cara e coroa!';

export default class SobreJogo extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{textPrincipal.toUpperCase()}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#61BD8C',
    flex: 1
  },
  text: {
    textAlign: 'center',
    marginTop: 15,
    fontSize: 16,
    color: '#FFF'
  }
});
