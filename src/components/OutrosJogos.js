import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

const textPrincipal = 'Outros jogos disponiveis so aqui!';

export default class OutrosJogos extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{textPrincipal.toUpperCase()}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#61BD8C',
    flex: 1
  },
  text: {
    textAlign: 'center',
    marginTop: 15,
    fontSize: 16,
    color: '#FFF'
  }
});
