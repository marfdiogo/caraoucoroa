import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

const textPrincipal = 'Resultado do jogo!';

const moedaCara = require('../imgs/moeda_cara.png');
const moedaCoroa = require('../imgs/moeda_coroa.png');

export default class Resultado extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resultado: ''
    };
  }

  componentWillMount() {
    const numAleatorio = Math.floor(Math.random() * 2);

    let caraOuCoroa = '';

    switch (numAleatorio) {
      case 0:
        caraOuCoroa = 'cara';
        break;
      case 1:
        caraOuCoroa = 'coroa';
        break;
      default:
        return false
    }

    this.setState({
      resultado: caraOuCoroa
    });
  }

  render() {
    if (this.state.resultado === 'cara') {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>{textPrincipal.toUpperCase()}</Text>
          <Text style={[styles.text, {fontSize: 30, fontWeight: 'bold', color: '#544026'}]}>{'Cara'.toUpperCase()}</Text>
          <View style={styles.moeda}>
            <Image source={moedaCara}/>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{textPrincipal.toUpperCase()}</Text>
        <Text style={[styles.text, {fontSize: 30, fontWeight: 'bold', color: '#544026'}]}>{'Coroa'.toUpperCase()}</Text>
        <View style={styles.moeda}>
          <Image source={moedaCoroa}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#61BD8C',
    flex: 1
  },
  text: {
    textAlign: 'center',
    marginTop: 15,
    fontSize: 16,
    color: '#FFF'
  },
  moeda: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  }
});
